# Contributing to DEMLab

Anyone is welcome to contribute to the project in one of the ways described below.

Please note that this project is released with a [Code of Conduct][code_conduct_link].
By participating in this project you agree to abide by its terms.

## Proposals

It includes proposing a change/new feature or report a bug.

Open an [issue][issue_link] through the GitLab interface.

Describe the proposal/bug clearly, preferably including reproducible examples, illustrations, or outputs.

Alternatively, you can contact the [authors][authors_link] by email.

## Implementations

It includes implementing a new feature, change an existing one, or fix a bug.

To modify the source code or files, create a [new branch][new_branch_link] from the master branch, and work on that branch to make the modifications.

Please check that all [tests][tests_link] are passing after finishing the modifications to the source code.
In addition, it is recommended to create new models for testing the implemented features.

Always try to keep the code organized.
It includes choosing clear and representative names for functions and variables,
paying attention to alignments and spacing,
and avoiding leaving unused code blocks as comments.

After the implementations are complete, make a [merge request][merge_request_link].
You should clearly describe the modifications and their purposes in order to facilitate understanding by others.

The merge of new branches into the master branch is subjected to the authors' approval.

You can also contact the [authors][authors_link] by email to discuss the modifications in advance.

[code_conduct_link]:  https://gitlab.com/rafaelrangel/demlab/-/blob/master/CODE_OF_CONDUCT.md
[issue_link]:         https://gitlab.com/rafaelrangel/demlab/-/issues/new
[authors_link]:       https://gitlab.com/rafaelrangel/demlab#authorship
[new_branch_link]:    https://gitlab.com/rafaelrangel/demlab/-/branches/new
[tests_link]:         https://gitlab.com/rafaelrangel/demlab#testing
[merge_request_link]: https://gitlab.com/rafaelrangel/demlab/-/merge_requests/new